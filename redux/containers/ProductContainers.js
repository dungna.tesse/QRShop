import HomeScreen from '../../src/screens/HomeScreen';
import {add_to_product} from '../actions';
import {connect} from 'react-redux';
const mapStateToProps = (state) =>{
    return {
       product : state.addProductReducers
    }
}
const mapDispatchToProps = (dispatch)=>{
    return {
        onAddProduct : ( data)=>{
            return dispatch(add_to_product(data))
        }
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(HomeScreen);