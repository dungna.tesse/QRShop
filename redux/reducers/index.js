import {combineReducers} from 'redux';
import addProductReducers from './addProductReducers'

const allReducers = combineReducers({
    addProductReducers
})
export default allReducers;