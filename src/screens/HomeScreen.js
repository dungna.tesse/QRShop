import * as React from 'react';
import {
  StyleSheet,
  Text,
  Modal,
  TextInput,
  View,
  SafeAreaView,
  TouchableOpacity,
  Image,
  Dimensions,
  ScrollView,
  Switch,
  Platform,
  FlatList,
} from 'react-native';

import {HomeData} from '../data/Data';
import {SamSungData} from '../data/Data';
const BannerWidth = Dimensions.get('window').width;
const BannerHeight = BannerWidth * 0.5;
const widthWindow = Dimensions.get('window').width;
const images = [
  'https://salt.tikicdn.com/cache/w584/ts/banner/e6/dc/38/1e1e71af648761ea4dc0e904a95bc629.jpg',
  'https://salt.tikicdn.com/cache/w584/ts/banner/d9/24/69/ead49d4a7d6f3c8cb1546271d2f0e069.jpg',
  'https://salt.tikicdn.com/cache/w584/ts/banner/3b/f8/cd/659222cc127e6b4fc8c986d57180eb88.jpg',
];
import Icon from 'react-native-vector-icons/FontAwesome';

export default function HomeScreen(props) {
  const {navigation} = props;
  const [isActive, setIsActive] = React.useState(0);
  const changeHandle = (event) => {
    const slide = Math.ceil(
      event.nativeEvent.contentOffset.x /
        event.nativeEvent.layoutMeasurement.width,
    );
    if (slide != isActive) {
      setIsActive(slide);
    }
  };
  const [newdata , setdata] =React.useState({})
  const [modalVisible, setModalVisible] = React.useState(false);

  const MyModal = ({item}) => {
    return (
      <Modal
        transparent={false}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}>
        <View style={styles.modal}>
          <View style={styles.modalHeader}>
            <Text style={styles.title}>QR code</Text>
            <View style={styles.divider}></View>
          </View>
          <View style={styles.modalBody}>
            <Image source= {{uri : item.qrcode }} resizeMode="contain" style={{width :200,height:200}}/>
          </View>
          <View style={styles.modalFooter}>
            <View style={styles.divider}></View>
            <View style={{flexDirection: 'row-reverse', margin: 10}}>
              <TouchableOpacity
                style={{...styles.actions, backgroundColor: '#db2828'}}
                onPress={() => {
                  setModalVisible(!modalVisible);
                }}>
                <Text style={styles.actionText}>Close</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  const _renderItem = ({item}) =>  {
    return (
    <TouchableOpacity
      style={styles.cardContent}
      onPress={() => {
      }}>
      <Image
        source={{uri: item.image}}
        style={{height: 50, width: 50}}
        resizeMode={'contain'}
      />
    </TouchableOpacity>

  )
};
  React.useEffect(() => {
    // console.log(props.product)
  }, []);
  const _renderActivity = ({item}) => { 
    return(    
    <TouchableOpacity
      style={{...styles._cardItem, alignItems: 'center'}}
      onPress={() => {
        // console.log(item)
        setModalVisible(true);
        setdata(item);

        // props.onAddProduct(item);
        // console.log( props.mapDispatchToProps(item));
        // navigation.navigate('mycart' ,{
        //   data :  item,
        //   screen:"product",
        // })
      }}>
         <MyModal item={newdata} />
      

      <Image
        source={{uri: item.image}}
        style={{height: 40, width: 40}}
        resizeMode={'contain'}
      />
      <Text style={{textAlign: 'center', marginVertical: 10}}>
        {item.title}
      </Text>
    </TouchableOpacity>

  )};
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.searchBar}>
        <View style={styles.search}>
          <Icon name="search" size={20} color="black" />
          <TextInput placeholder={'Search...'} style={styles.searchText} />
          <TouchableOpacity 
          onPress={()=>{
            navigation.navigate('qrcode');
          }}
            style={{position: 'absolute', right: 10, zIndex: 1}}>
            <Icon name="qrcode" size={25} color="black" />
          </TouchableOpacity>
        </View>
      </View>
      <ScrollView>
        <View>
          <ScrollView
            pagingEnabled
            horizontal
            onScroll={changeHandle}
            style={{width: BannerWidth, height: BannerHeight}}
            showsHorizontalScrollIndicator={false}>
            {images.map((val, index) => {
              return (
                <Image
                  key={index}
                  source={{uri: val}}
                  style={{
                    width: BannerWidth,
                    height: BannerHeight,
                    resizeMode: 'cover',
                  }}
                />
              );
            })}
          </ScrollView>
          <View style={styles.pagination}>
            {images.map((item, index) => {
              return (
                <Icon
                  name="circle"
                  size={20}
                  key={index}
                  style={
                    index === isActive
                      ? styles.paginationtext
                      : styles.paginationActivetext
                  }
                  ize={8}
                />
              );
            })}
          </View>
        </View>
        <View style={styles._card}>
          <View style={styles._cardItem}>
            <View style={{position: 'relative'}}>
              <Image
                source={{
                  uri:
                    'https://1000logos.net/wp-content/uploads/2017/02/iPhone_logo.png',
                }}
                style={{height: 20, width: 20, position: 'absolute', left: 0}}
                resizeMode={'contain'}
              />
              <Text style={{left: 30, position: 'absolute'}}>Iphone</Text>
            </View>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('mycart', {
                  screen: 'shop',
                });
              }}
              style={{
                position: 'absolute',
                right: 5,
                top: 0,
                borderRadius: 10,
                backgroundColor: 'lightgray',
                zIndex: 1,
                paddingHorizontal: 10,
                paddingVertical: 4,
              }}>
              <Text>See all</Text>
            </TouchableOpacity>
          </View>

          <FlatList
            style={{marginTop: 20}}
            data={HomeData}
            renderItem={_renderActivity}
            keyExtractor={(item) => item.id}
            horizontal={true}
          />
        </View>
        <View style={styles._card}>
          <View style={styles._cardItem}>
            <View style={{position: 'relative'}}>
              <Image
                source={{
                  uri:
                    'https://img2.thuthuatphanmem.vn/uploads/2018/12/13/logo-samsung-moi_033419539.jpg',
                }}
                style={{
                  height: 60,
                  width: 60,
                  top: -15,
                  position: 'absolute',
                  left: 0,
                }}
                resizeMode={'contain'}
              />
            </View>

            <TouchableOpacity
              style={{
                position: 'absolute',
                right: 5,
                top: 0,
                borderRadius: 10,
                backgroundColor: 'lightgray',
                zIndex: 1,
                paddingHorizontal: 10,
                paddingVertical: 4,
              }}
              onPress={() => {
                navigation.navigate('mycart', {
                  screen: 'shop',
                });
              }}>
              <Text>See all</Text>
            </TouchableOpacity>
          </View>

          <FlatList
            style={{marginTop: 20}}
            data={SamSungData}
            renderItem={_renderActivity}
            keyExtractor={(item) => item.id}
            horizontal={true}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  timeSeen: {
    position: 'absolute',
    right: 20,
    bottom: 8,
    fontSize: 10,
  },
  time: {
    position: 'absolute',
    right: 20,
    bottom: 8,
    fontSize: 10,
    fontWeight: 'bold',
  },
  textHeader: {
    fontSize: 16,
    fontWeight: 'bold',
    marginLeft: 70,
  },
  textContent: {
    fontSize: 14,
    marginLeft: 70,
    fontWeight: 'bold',
  },
  textSeen: {
    fontSize: 14,
    marginLeft: 70,
  },
  searchBar: {
    height: 55,
    backgroundColor: 'white',
    justifyContent: 'center',
    paddingHorizontal: 5,
    marginVertical: 5,
  },
  searchText: {
    fontSize: 15,
    marginHorizontal: 15,
    width: '80%',
    justifyContent: 'center',
    borderColor: 'transparent',
  },
  search: {
    height: 55,
    backgroundColor: '#f2f0f5',
    flexDirection: 'row',
    padding: 5,
    alignItems: 'center',
    borderRadius: 6,
    elevation: 3,
    shadowOffset: {width: 1, height: 1},
    shadowOpacity: 0.3,
  },
  container: {
    flex: 1,
    // paddingTop: Platform.OS === 'android' ? 25 : 0,
    backgroundColor: 'white',
    // marginTop: 30,
    // width: '100%'
  },
  card: {
    borderRadius: 6,
    elevation: 3,
    backgroundColor: '#fff',
    shadowOffset: {width: 1, height: 1},
    textShadowColor: '#333',
    shadowOpacity: 0.3,
    textShadowRadius: 0.3,
    marginHorizontal: 5,
    marginVertical: 20,
    flexDirection: 'row',
    paddingVertical: 20,
    paddingHorizontal: 15,
    borderTopWidth: 5,
    borderTopColor: 'lightgray',
  },
  cardContent: {
    // marginHorizontal: 20,
    // marginVertical: 20,
    // borderRadius: 6,
    // elevation: 3,
    // backgroundColor: 'transparent',
    // shadowOffset: { width: 1, height: 1 },
    // textShadowColor: 'transparent',
    // shadowOpacity: 0.3,
    // textShadowRadius: 0.3,
    flexDirection: 'column',
    marginHorizontal: 5,
    marginVertical: 5,
    flex: 1,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    // borderWidth:1,
    paddingVertical: 15,
    // borderColor  :'lightgray'
  },
  _cardItem: {
    marginHorizontal: 18,
    marginVertical: 10,
  },

  _card: {
    borderRadius: 6,
    elevation: 3,
    backgroundColor: '#fff',
    shadowOffset: {width: 1, height: 1},
    textShadowColor: '#333',
    shadowOpacity: 0.3,
    textShadowRadius: 0.5,
    marginHorizontal: 4,
    marginVertical: 6,
    zIndex: 1,
    borderTopWidth: 5,
    borderTopColor: 'lightgray',
  },
  pagination: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    alignSelf: 'center',
    zIndex: 1,
    backgroundColor: 'transparent',
  },
  paginationtext: {
    color: 'red',
    fontSize: 9,
    marginHorizontal: 1,
  },
  paginationActivetext: {
    color: 'gray',
    fontSize: 9,
    marginHorizontal: 1,
  },
  modal: {
    backgroundColor: '#00000099',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalContainer: {
    backgroundColor: '#f9fafb',
    // width:"100%",
    borderRadius: 5,
    width: widthWindow - 50,
  },
  modalHeader: {},
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    padding: 15,
    color: '#000',
  },
  divider: {
    width: '100%',
    height: 1,
    backgroundColor: 'lightgray',
  },
  modalBody: {
    backgroundColor: '#fff',

    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  modalFooter: {},
  actionText: {
    color: '#fff',
    padding: 3,
  },
});
