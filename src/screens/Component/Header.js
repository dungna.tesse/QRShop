import React from 'react';
import {View, Text, StyleSheet, TextInput, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

const Header = (props) => {
  const {navigation} = props;
  return (
    <View style={styles.top}>
        <TextInput style={styles.input} placeholder="Search in store..."/>
        <TouchableOpacity style={{flexDirection: 'row', position: 'absolute', right: 5, top: 15}} onPress={()=>{
          navigation.navigate('Home')
        }}>
            <Icon name="qrcode" size={30} color='white' style={{marginHorizontal: 5}}/>
            <Text style={styles.logo}>QRShop</Text>
        </TouchableOpacity>
    </View>

  )
}

const styles = StyleSheet.create({
    top: {
        height: 60,
        padding: 15,
        backgroundColor: '#297945',
        flexDirection: 'row',
        position: 'relative',
    },
    input:{
        borderRadius: 20,
        width: 200,
        padding: 8,
        backgroundColor: '#D3D3D3D3',
        fontSize: 10,
        justifyContent: 'center',
    },
    logo: {
      color: 'white',
      fontSize: 25,
    },
})
export default Header;
