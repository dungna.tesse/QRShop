import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity, ScrollView} from 'react-native';
import Header from './Component/Header'
import Icon from 'react-native-vector-icons/FontAwesome5';

const DATA = [
  {
    id: '1',
    title: 'Samsung Galaxy A71',
    uri: require('./Images/galaxy-a71.jpg'),
    price: '7.999.000',
    camera: '64 MP, f/1.8, 26mm (wide), 1/1.72", 0.8µm, PDAF',
    display: 'Super AMOLED Plus capacitive touchscreen, 16M colors',
    body: '163.6 x 76 x 7.7 mm (6.44 x 2.99 x 0.30 in)',
    memory: 'microSDXC (dedicated slot)',
    battery: 'Li-Ion 4500 mAh, non-removable',
    OS: 'Android 10, One UI 2.1',
  },
];

const Shops = (props) => {
    // const {params} = props.navigation.state;
    const { navigation }  = props;
    console.log(props)
    // const userData = navigation.getParam('data', null);  

  return (
    <View style={styles.container}>
        <Header {...props}/>
        <ScrollView>
            <View style={{flexDirection: 'row', alignItems:'center', justifyContent: 'space-evenly'}}>
                <Image source={DATA[0].uri} style={styles.product} resizeMode="contain"/>
                <View style={{padding: 20, textAlign: 'center'}}>
                    <Text style={styles.title}>{DATA[0].title}</Text>
                    <Text style={{paddingBottom: 10}}>Price: {DATA[0].price} đ</Text>
                    <TouchableOpacity style={{backgroundColor: 'red', padding: 10, flexDirection: 'row', justifyContent: 'space-evenly'}}>
                        <Text style={{textAlign:'center', color: 'white'}}>Add to cart</Text>
                        <Icon name="cart-plus" color='white' size={20}/>
                    </TouchableOpacity>
                </View>
            </View>
            <Text style={{textAlign: 'center', fontSize: 20, fontWeight: 'bold', padding: 10}}>SPECIFICATION</Text>
            <View style={{marginHorizontal :20 , justifyContent:"center", alignItems:"flex-start" , marginRight :50}}>
                <View style={{flexDirection: 'row'}}>
                    <Text style={{fontWeight: 'bold'}}>Camera: </Text>
                    <Text>{DATA[0].camera}</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <Text style={{fontWeight: 'bold'}}>Display: </Text>
                    <Text>{DATA[0].display}</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <Text style={{fontWeight: 'bold'}}>Body: </Text>
                    <Text>{DATA[0].body}</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <Text style={{fontWeight: 'bold'}}>Memory: </Text>
                    <Text>{DATA[0].memory}</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <Text style={{fontWeight: 'bold'}}>Battery: </Text>
                    <Text>{DATA[0].battery}</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <Text style={{fontWeight: 'bold'}}>Operating System: </Text>
                    <Text>{DATA[0].OS}</Text>
                </View>
            </View>
        </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
    container: {
        flex:1,
    },
    product:{
      width: 150,
      height: 250,
      padding: 20,
      marginLeft: 10,
    },
    title: {
        fontSize: 20,
        paddingBottom: 50,
        fontWeight: 'bold',
    }
})
export default Shops;
