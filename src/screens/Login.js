import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  Dimensions,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  TouchableWithoutFeedback,
  Keyboard,
  KeyboardAvoidingView,
  ImageBackground
} from 'react-native';

const {width: WIDTH} = Dimensions.get('window');

import Icon from 'react-native-vector-icons/FontAwesome';

export default function LoginScreen({children, ...props}) {
  const {navigation} = props;
  const [showPassword, setShow] = useState(true);
  const [press, setPress] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isLoading, setIsloading] = React.useState(false);

  // const onRegisterPress = () => {
  //   firebase
  //     .auth()
  //     .createUserWithEmailAndPassword(email + '@gmail.com', password)
  //     .then((response: any) => {
  //       // addUser(data.user.uid);
  //       const uid = response.user.uid
  //       const data = {
  //         id: uid,
  //         email: email + "@gmail.com",
  //         username: email,
  //         fullname: "Le Van Nhat",
  //         color: "red",
  //         address: "HCM city",
  //         birthday: '06/02/1999',
  //         role: "user"
  //       };
  //       const usersRef = firebase.firestore().collection('users')
  //       usersRef
  //         .doc(uid)
  //         .set(data)
  //         .then(() => {
  //           navigation.navigate("Root")
  //         })
  //         .catch((error) => {
  //           alert(error)
  //         });

  //     })
  //     .catch(error => alert(JSON.stringify(error.message)));
  // }
  const onHandleLogin = () => {
    // props.onPostLogin({ email: email, password: password })
    setIsloading(true);
              setTimeout(() => {
                navigation.navigate('MyHome');
                setIsloading(false);
     }, 1000)
  }
  // React.useEffect(() => {
  //   if (props.payload.status === 200) {
  //     props.onGetUser(props.payload.user.uid);
  //     Alert.alert(
  //       "Notification",
  //       "Login is successfully !",
  //       [
  //         {
  //           text: "OK", onPress: () => {
  //             setIsloading(true);
  //             setTimeout(() => {
  //               navigation.navigate('Root');
  //               setIsloading(false);
  //             }, 3000)

  //           }
  //         }
  //       ],
  //       { cancelable: false }
  //     );

  //   }
  //   if (props.payload.status === 503) {
  //     alert(JSON.stringify(props.payload.error.message))
  //   }

  // }, [
  //   props.payload,
  // ])
  const inputEl2 = React.useRef(null);

  return (
    <View style={styles.container}>
      {isLoading ? (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator size="large" color={'red'}/>
          <Text>Loading 99%</Text>
        </View>
      ) : (
        <ImageBackground source={{uri : "https://anhdepfree.com/wp-content/uploads/2019/05/50-anh-background-dep-nhat.png"}} style={{
         
        }}>
          <TouchableWithoutFeedback
            onPress={Keyboard.dismiss}
            accessible={false}>
               <KeyboardAvoidingView
          style={{
            flex: 1,
          }}
          behavior="padding">
            <View style={styles.container}>
              {/* <Image source={require('../assets/images/reactlogo.png')} style={styles.logo} resizeMode={"contain"} /> */}
              <Text style={styles.welcome}>Welcome to QRShop</Text>
              <View
                style={{
                  marginBottom: 20,
                  // marginTop: 30,
                  backgroundColor: 'transparent',
                }}>
                <Icon name="user-circle" size={24} style={styles.userIcon} />
                <TextInput
                  style={styles.username}
                  placeholder="User Name"
                  placeholderTextColor={'#1e90ff'}
                  underlineColorAndroid="transparent"
                  onChangeText={(text) => setEmail(text)}
                  value={email}
                  returnKeyType="next"
                  onSubmitEditing={() => inputEl2.current.focus()}
                />
              </View>

              <View style={{backgroundColor: 'transparent'}}>
                <Icon name="key" size={24} style={styles.userIcon} />
                <TextInput
                  ref={inputEl2}
                  style={styles.username}
                  placeholder="Password"
                  placeholderTextColor={'#1e90ff'}
                  underlineColorAndroid="transparent"
                  secureTextEntry={showPassword}
                  onChangeText={(text) => setPassword(text)}
                  value={password}
                  onSubmitEditing={onHandleLogin}
                />
              </View>
              <View style={{backgroundColor: 'transparent'}}>
                <TouchableOpacity style={styles.btnLogin} onPress={onHandleLogin}>
                  <Text style={styles.btnTitle}>Login</Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  marginTop: 10,
                  alignItems: 'flex-start',
                  marginLeft: 0,
                  backgroundColor: 'transparent',
                }}>
                <TouchableOpacity onPress={()=>{
                  navigation.navigate('Register')
                }}>
                  <Text style={{textAlign: 'left', color: '#297945'}}>
                    Dont' have an account ? Sign up here!
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
        </KeyboardAvoidingView>
          </TouchableWithoutFeedback>
          </ImageBackground>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  welcome: {
    fontSize: 60,
    width: 400,
    color: '#297945',
    textAlign: 'center',
    padding: 50,
    fontWeight: 'bold',
    fontFamily: 'Roboto',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: 'white',
    
  },
  logo: {
    width: 200,
    height: 200,
  },
  username: {
    borderColor: '#1e90ff',
    width: WIDTH - 60,
    height: 40,
    borderRadius: 45,
    fontSize: 16,
    paddingLeft: 45,
    // color : "#1e90ff",
    color: 'black',
    backgroundColor: '#dcdcdc',
    marginHorizontal: 25,
  },
  password: {
    borderColor: '#1e90ff',
    width: WIDTH - 60,
    height: 40,
    borderRadius: 45,
    fontSize: 16,
    paddingLeft: 45,
    color: '#1e90ff',
    backgroundColor: '#dcdcdc',
    marginHorizontal: 25,
  },
  userIcon: {
    position: 'absolute',
    top: 7,
    left: 35,
    zIndex: 1,
  },
  btnEye: {
    position: 'absolute',
    top: 8,
    right: 35,
  },
  btnLogin: {
    marginTop: 20,
    borderColor: '#1e90ff',
    width: WIDTH - 60,
    height: 40,
    borderRadius: 45,
    fontSize: 16,
    color: '#1e90ff',
    backgroundColor: '#297945',
    marginHorizontal: 25,
    justifyContent: 'center',
  },
  btnTitle: {
    fontSize: 16,
    textAlign: 'center',
    alignItems: 'center',
    color: 'white',
  },
});
