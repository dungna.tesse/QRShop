import React, {useState} from 'react';
import {View, Text, StyleSheet, FlatList, CheckBox, Image, ScrollView, TouchableOpacity} from 'react-native';
import Header from './Component/Header'
import Icon from 'react-native-vector-icons/FontAwesome5';

const Item = (props) => {
    const { title, image, price, deleteFunc, id } = props;
    const [isSelected, setSelection] = useState(false);

    return(
    <View>
      <ScrollView>
        <View style={{justifyContent: 'center', alignItems: 'center', padding: 10, flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <CheckBox
                value={isSelected}
                onValueChange={setSelection}
                style={{padding: 20}}
            />
            <Image style={styles.product} source={image}/>
            <View style={{padding: 10}}>
                <Text style={{textAlign: 'left', fontSize: 15, padding: 5}}>{title}</Text>
                <Text style={{textAlign: 'left', fontSize: 10, padding: 5}}>Price: {price} đ</Text>
            </View>
            <TouchableOpacity onPress={()=>deleteFunc(id)} style={{width: 30, height: 30, justifyContent: 'center', alignItems: 'center'}}>
                <Icon name="times" color='red' size={20}/>
            </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
)};

const Cart = (props) => {
  const renderItem = ({ item }) => (
    <Item title={item.title}  image={item.uri} price={item.price} deleteFunc={deleteItem} id={item.id}/>
  );

  const [DATA, setDATA] = useState ([
    {
      id: '1',
      title: 'Samsung Galaxy A12',
      uri: require('./Images/samsung-galaxy-a21.jpg'),
      price: '3.850.000'
    },
    {
      id: '2',
      title: 'Samsung Galaxy A71',
      uri: require('./Images/galaxy-a71.jpg'),
      price: '7.999.000'
    },
    {
      id: '3',
      title: 'Samsung Galaxy S10',
      uri: require('./Images/s10plus-min.jpg'),
      price: '8.290.000'
    },
  ]);

  const deleteItem = (id) => {
    setDATA(prevItem => {
        return prevItem.filter(item => item.id != id)
    });
  }

  return (
    <View style={styles.container}>
        <Header {...props}/>
        <FlatList style={{alignContent: 'center'}}
          data={DATA}
          renderItem={renderItem}
        />
        <TouchableOpacity style={{alignContent: 'center', justifyContent: 'center', backgroundColor: 'red', padding: 15, marginBottom: 0}}>
            <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'white', fontSize: 25}}>Checkout</Text>
        </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
    container: {
        flex:1,
    },
    product:{
      width: 30,
      height: 50,
    }
})

export default Cart;
