import * as React from 'react';
import {Text, View , StyleSheet, SafeAreaView, TouchableOpacity, Image, Dimensions, ScrollView, Platform, Linking, Alert } from 'react-native';

const BannerWidth = Dimensions.get('window').width;

const BannerHeight = BannerWidth * 0.5;
import Icon from 'react-native-vector-icons/FontAwesome';

export default function UserScreen(props) {
  const { navigation } = props;
  
  return (
    <ScrollView style={{
      // paddingTop: Platform.OS === 'android' ? 25 : 0,
      backgroundColor: "white",
      flex: 1
    }}>
      <View>
        <Image source={{ uri: "https://cf.shopee.vn/file/37de468b5279c81d3df40175075831ef_xxhdpi" }} style={{ width: BannerWidth, height: BannerHeight, resizeMode: 'cover' }} />
        <View style={{ justifyContent: "center" }} >
          <Image
            style={styles.image}
            resizeMode={"cover"}
            source={{ uri: 'https://meovatcuocsong.vn/wp-content/uploads/2019/03/avatar-facebook-dep-31.jpg' }}
          />
        </View>
        <TouchableOpacity style={{ justifyContent: "center", alignSelf: "center", left: 40, top: 10 }}
         
        >
          {/* <Feather name="camera" size={20} color="black" /> */}
        </TouchableOpacity>

      </View>

      <ScrollView>
        <View style={styles.container}>
          <TouchableOpacity style={styles.separator}>
          <Icon name="user-circle" style={styles.iconleft} size={24} color="gray" />
            <Text style={styles.text}>Nhật đẹp trai</Text>
            <TouchableOpacity style={styles.icon}>
              <Icon name="chevron-right"size={10} color="black" />
            </TouchableOpacity>

          </TouchableOpacity>

          <View style={styles.separator}>
          <Icon name="truck" style={styles.iconleft} size={24} color="gray" />
            <Text style={styles.text}>Thông tin đơn hàng</Text>
          </View>
         
          <View style={styles.separator}>
          <Icon name="birthday-cake" style={styles.iconleft} size={24} color="gray" />
            <Text style={styles.text}>1999-02-06</Text>
          </View>

          <TouchableOpacity style={styles.separator}>
            <Icon name="venus-mars" style={styles.iconleft} size={24} color="gray" />
            <Text style={styles.text}>Nam</Text>

          </TouchableOpacity>
          <View style={styles.separator}>
            <Icon name="envelope-open" style={styles.iconleft} size={20} color="gray" />
            <Text style={styles.text}>email.nhatle123gamil.com</Text>
          </View>
          <TouchableOpacity style={styles.separator} onPress={()=>{
                navigation.navigate('Login')
            }}>
            <Icon name="sign-out" style={styles.iconleft} size={20} color="gray"  />
            <Text style={styles.text} >Logout</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </ScrollView>

  );
}

const styles = StyleSheet.create({
  textHeader: { 
    fontSize: 16,
    fontWeight: "bold",
    marginLeft: 70
  },
  image: {
    height: 90,
    width: 90,
    borderRadius: 60,
    position: "absolute",
    left: BannerWidth / 2 - 40,
    borderWidth: 1,
    borderColor: "lightgray",
    zIndex: 1

  },
  textContent: {
    fontSize: 14,
    marginLeft: 70,
    fontWeight: "bold",
  },
  container: {
    width: "100%",
    flex: 1,
    alignItems: 'center',
    // justifyContent: 'center',      
    marginVertical: 50,
    backgroundColor: "white"

  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
    alignItems: 'center',
    textAlign: "center"

  },
  separator: {
    // marginVertical: 20,
    height: 60,
    width: '90%',
    borderWidth: 1,
    borderTopColor: "transparent",
    borderRightColor: "transparent",
    borderLeftColor: "transparent",
    borderBottomColor: "lightgray",
    justifyContent: "center",
    backgroundColor: "white",
  },
  icon: {
    position: "absolute",
    right: 20
  },
  iconleft: {
    position: "absolute",
    left: 0,
  },
  text: {
    marginLeft: 50
  },
  colors: {
    position: "absolute",
    right: 0,
    width: 15,
    height: 15,
    backgroundColor: "red",
    justifyContent: "center",
  },
  pagination: {
    flexDirection: "row",
    position: "absolute",
    bottom: 0,
    alignSelf: "center",
    zIndex: 1,
    backgroundColor: "transparent"
  },
  paginationtext: {
    color: "red",
    fontSize: 9,
    marginHorizontal: 1
  },
  paginationActivetext: {
    color: "gray",
    fontSize: 9,
    marginHorizontal: 1
  }
});