import React from 'react';
import {View, Text, StyleSheet, FlatList, TextInput, Image, TouchableOpacity} from 'react-native';
import Header from './Component/Header';
import Icon from 'react-native-vector-icons/FontAwesome5';

const DATA = [
  {
    id: '1',
    title: 'Samsung Galaxy A12',
    uri: require('./Images/samsung-galaxy-a21.jpg'),
    price: '3.850.000'
  },
  {
    id: '2',
    title: 'Samsung Galaxy A71',
    uri: require('./Images/galaxy-a71.jpg'),
    price: '7.999.000'
  },
  {
    id: '3',
    title: 'Samsung Galaxy S10',
    uri: require('./Images/s10plus-min.jpg'),
    price: '8.290.000'
  },
];

const Item = (props) => {
  const { title, image, price } = props;
  const {navigation} = props;
  return (
    <View>
      <TouchableOpacity style={{justifyContent: 'space-evenly', alignItems: 'center', padding: 10, flexDirection: 'row'}} onPress={()=>{
        navigation.navigate('product')
      }}>
        <Image style={styles.product} source={image} resizeMode="contain"/>
        <View style={{padding: 10}}>
          <Text style={{textAlign: 'left', fontSize: 22, padding: 10}}>{title}</Text>
          <Text style={{textAlign: 'left', fontSize: 12, padding: 10}}>Price: {price} đ</Text>
        </View>
        <View style={{marginLeft: -50, marginBottom: -50, flexDirection: 'row',justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{fontSize: 8, padding: 5}}>See details</Text>
          <Icon name="arrow-right" size={8}/>
        </View>
      </TouchableOpacity>
    </View>
);}

const Shops = (props) => {
  const {navigation} = props;
  const renderItem = ({ item }) => (
    <Item title={item.title} {...props} image={item.uri} price={item.price}/>
  );

  return (
    <View style={styles.container}>
        <Header {...props } />
        <FlatList style={{alignContent: 'center'}}
          data={DATA}
          renderItem={renderItem}
          keyExtractor={item => item.id}
        />
    </View>
  );
};

const styles = StyleSheet.create({
    container: {
        flex:1,
    },
    product:{
      width: 50,
      height: 80,
    }
})
export default Shops;
