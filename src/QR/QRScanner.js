import React from 'react';
import QRCodeScanner from 'react-native-qrcode-scanner';


const QRScanner = (props) => {
  const {navigation} = props;

  const onSuccess = e => {
    navigation.navigate('shop');
  }

  return (
    <QRCodeScanner
      onRead={onSuccess}
    />
  );

}

export default QRScanner;
