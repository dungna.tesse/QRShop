/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
// import Ionicons from 'react-native-vector-icons/Ionicons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import HomeScreen from '../screens/HomeScreen';
import Icon from 'react-native-vector-icons/FontAwesome';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../screens/Login';
import RegisterScreen from '../screens/Register';
import UserScreen from '../screens/UserScreen';
import ShopScreen from '../screens/Shops';
import ProductPageScreen from '../screens/ProductPage';
import ShopingCartScreen from '../screens/ShoppingCart';
import QrCodeSceen from '../QR/QRScanner';

const Tab = createBottomTabNavigator();
const  Tabnavigation = () => {
  return (
    
      <Tab.Navigator
      initialRouteName="Home"
        tabBarOptions={{ activeTintColor: 'blue'  }}        
      >
        <Tab.Screen 
        options={{
          tabBarIcon: ({ color }) =>  <Icon name="home" size={20}  color='gray' />,
          tabBarLabel: "Home",
          
        }} 
        name="Home" component={HomeScreen} 
        />
        
        <Tab.Screen 
        options={{
          tabBarIcon: ({ color }) =>  <Icon name="shopping-cart" size={20} color='gray' />,
          tabBarLabel: "Cart"
        }} name="mycart" component={MystackNavigator} 
        />
        <Tab.Screen 
        options={{  
          tabBarIcon: ({ color }) =>  <Icon name="user" size={20} color='gray' />,
          tabBarLabel: "User"
        }} name="user" component={UserScreen} 
        />
        
      </Tab.Navigator>
  );
};
const Mystack = createStackNavigator();
const MystackNavigator = () =>{
 return(
  <Mystack.Navigator
  initialRouteName="shoppingcart"
    screenOptions={{
      headerShown: false
    }}
    >
  <Mystack.Screen
    name="shop"
    component={ShopScreen}
    // options={{
    //   header: ({ scene, previous, navigation }: any) => {
    //     return (
    //       <Header navigation={navigation} title={'Color Info'} status={0} />

    //     )
    //   }
    // }}
  />
  <Mystack.Screen
     options={{
      tabBarIcon: ({ color }) =>  <Icon name="shopping-cart" size={20} color='gray' />,
      tabBarLabel: "Cart"
    }} 
    name="shoppingcart" component={ShopingCartScreen} 
  />
  <Mystack.Screen
    name="product"
    component={ProductPageScreen}
    // options={{
    //   header: ({ scene, previous, navigation }: any) => {
    //     return (
    //       <Header navigation={navigation} title={'Color Info'} status={0} />

    //     )
    //   }
    // }}
  />
  
</Mystack.Navigator>
 )

}
const MyAuth = createStackNavigator();
const MyAuthNavigator =() => {
  return (
    <NavigationContainer >
    <MyAuth.Navigator
      initialRouteName="Login"
    screenOptions={{
      headerShown: false
    }}
    >
      <MyAuth.Screen
        name="Login"
        component={LoginScreen}
        // options={{
        //   header: ({ scene, previous, navigation }) => {
        //     return (
        //       <Header navigation={navigation} title={'Setting'} status={0} />

        //     )
        //   }
        // }}
      />
      <MyAuth.Screen
        name="Register"
        component={RegisterScreen }
        // options={{
        //   header: ({ scene, previous, navigation }: any) => {
        //     return (
        //       <Header navigation={navigation} title={'Color Info'} status={0} />

        //     )
        //   }
        // }}
      />
      <MyAuth.Screen
        name="MyHome"
        component={Tabnavigation}
        // options={{
        //   header: ({ scene, previous, navigation }: any) => {
        //     return (
        //       <Header navigation={navigation} title={'Color Info'} status={0} />

        //     )
        //   }
        // }}
      />
      <MyAuth.Screen
        name="shop"
        component={ShopScreen}
        // options={{
        //   header: ({ scene, previous, navigation }: any) => {
        //     return (
        //       <Header navigation={navigation} title={'Color Info'} status={0} />

        //     )
        //   }
        // }}
      />
      <MyAuth.Screen
        name="product"
        component={ProductPageScreen}
        // options={{
        //   header: ({ scene, previous, navigation }: any) => {
        //     return (
        //       <Header navigation={navigation} title={'Color Info'} status={0} />

        //     )
        //   }
        // }}
      />
      <MyAuth.Screen
        name="qrcode"
        component={QrCodeSceen}
        // options={{
        //   header: ({ scene, previous, navigation }: any) => {
        //     return (
        //       <Header navigation={navigation} title={'Color Info'} status={0} />

        //     )
        //   }
        // }}
      />
      
    </MyAuth.Navigator>
    </NavigationContainer>
  )
}

export default MyAuthNavigator;
