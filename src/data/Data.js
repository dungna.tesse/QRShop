
export  const  HomeData = [
  {
    id: "1",
    title: "Iphone 6",
    image:
    "https://cdn.cellphones.com.vn/media/catalog/product/cache/7/image/9df78eab33525d08d6e5fb8d27136e95/i/p/iphone_6s.jpg",
    qrcode :"https://firebasestorage.googleapis.com/v0/b/uitapp-2357e.appspot.com/o/QRcode%2Fiphone6.png?alt=media&token=c6bdef67-7836-4b96-8211-851df565c94e"
  },
  {
    id: "2",
    title: "Iphone 7",
    qrcode :"https://firebasestorage.googleapis.com/v0/b/uitapp-2357e.appspot.com/o/QRcode%2Fiphone7.png?alt=media&token=58df412b-f2d0-4fe4-ad57-bd09f74831ed",
    image:
      "https://cdn.cellphones.com.vn/media/catalog/product/cache/7/image/9df78eab33525d08d6e5fb8d27136e95/i/p/iphone_8_256gb_2.jpg",
  },
  {
    id: "3",
    title: "Iphone 8",
    qrcode :"https://firebasestorage.googleapis.com/v0/b/uitapp-2357e.appspot.com/o/QRcode%2Fiphone8.png?alt=media&token=ed391210-1214-4c4e-8cd9-08a19feffda8",
    image:
      "https://cdn.cellphones.com.vn/media/catalog/product/cache/7/image/9df78eab33525d08d6e5fb8d27136e95/i/p/iphone_8_red.jpg",
  },
  {
    id: "4",  
    title: "Iphone X",
    qrcode :"https://firebasestorage.googleapis.com/v0/b/uitapp-2357e.appspot.com/o/QRcode%2Fiphone%20X.png?alt=media&token=dabb3e00-e99f-4a6a-8335-af820a388c48",
    image:
      "https://ctmobile.vn/upload/iphone%20xs%20max%20gray.jpg",

  },
  {
    id: "5",
    title: "Iphone 11",
    qrcode :"https://firebasestorage.googleapis.com/v0/b/uitapp-2357e.appspot.com/o/QRcode%2Fiphone%2011.png?alt=media&token=8edf4383-2121-49a7-a35d-3cafae35b388",
    image:
      "https://bachlongmobile.com/media/catalog/product/cache/1/image/040ec09b1e35df139433887a97daa66f/1/_/1_55_3_2_1_1.jpg",
    navigate: "SamSung"
  },
 
]


export  const  SamSungData = [
  {
    id: "1",
    title: "A51",
    qrcode :"https://firebasestorage.googleapis.com/v0/b/uitapp-2357e.appspot.com/o/QRcode%2FA51.png?alt=media&token=9048aad2-263c-4328-b1a1-8a64c6ab66b6",
    image:
      "https://salt.tikicdn.com/cache/w390/ts/product/0c/98/a8/a6a1dc2eec8189746bf75c824a6d64e9.jpg",
  },
  { 
    id: "2",
    title: "M21",
    qrcode :"https://firebasestorage.googleapis.com/v0/b/uitapp-2357e.appspot.com/o/QRcode%2FM21.png?alt=media&token=4b3d9dad-bcec-49d7-8c2d-c78f943a100b",
    image:
      "https://salt.tikicdn.com/cache/w390/ts/product/e9/4d/8c/7f3ca8af28dbf1281892c07f5c34dc25.jpg",
  },    
  {
    id: "3",
    title: "A71",
    image:
      "https://salt.tikicdn.com/cache/w390/ts/product/76/ad/66/b32de1ed6526261a4c728805c0163244.jpg",
    navigate: "SamSung",
    qrcode :  "https://firebasestorage.googleapis.com/v0/b/uitapp-2357e.appspot.com/o/QRcode%2FA71.png?alt=media&token=33de726f-6ffc-466a-9dac-5fe14d80bb1e"
  },
  {
    id: "4",  
    title: "Note 9",
    qrcode :"https://firebasestorage.googleapis.com/v0/b/uitapp-2357e.appspot.com/o/QRcode%2FNote%209.png?alt=media&token=dc234e96-3a0d-438a-907c-6735957cb76d",
    image:
      "https://salt.tikicdn.com/cache/w390/ts/product/47/81/d5/745995b000633ae0b1769e1573df3b52.jpg",

  },
  {
    id: "5",
    title: "Note 10",
    qrcode :"https://firebasestorage.googleapis.com/v0/b/uitapp-2357e.appspot.com/o/QRcode%2FNote%2010.png?alt=media&token=3ccd5ff9-fee3-4204-87e7-82e5c44348b6",
    image:
      "https://images.fpt.shop/unsafe/fit-in/465x465/filters:quality(90):fill(white)/fptshop.com.vn/Uploads/Originals/2020/1/17/637148516615700174_SS-note10-lite-den-1.png",
  },
  {
    id: "6",
    title: " S10 Plus ",
    qrcode :"https://firebasestorage.googleapis.com/v0/b/uitapp-2357e.appspot.com/o/QRcode%2FS10%20Plus.png?alt=media&token=a4a5b1b7-dd51-4a75-ac45-741cddeb1deb",
    image:
      "https://salt.tikicdn.com/cache/w390/ts/product/0a/69/19/060b0d6207b6d3f96cb90c2d43709d19.jpg",
    navigate: "SamSung"
  },
 
]
