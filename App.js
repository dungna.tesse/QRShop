import React from 'react';

// import Ionicons from 'react-native-vector-icons/Ionicons';
import { createStore, applyMiddleware } from 'redux';
import allReducers from './redux/reducers/index';
import { Provider } from 'react-redux'; 
const store = createStore(allReducers);

import TabNavigation from './src/navigations/tabnavigation'
const App = () => {
  return (   
    <Provider store={store}>
      <TabNavigation/>
    </Provider> 
      
  );
};  


export default App;
